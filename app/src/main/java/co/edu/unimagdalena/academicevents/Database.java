package co.edu.unimagdalena.academicevents;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;

public class Database extends SQLiteOpenHelper {
    public Database(@Nullable Context context, int version) {
        super(context, DefDB.nameDb, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(DefDB.create_tabla_est);
        db.execSQL(DefDB.create_tabla_evt);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL(DefDB.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

}
