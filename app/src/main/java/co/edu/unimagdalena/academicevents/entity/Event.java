package co.edu.unimagdalena.academicevents.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Event implements Parcelable {

    private String id, owner, speaker, title, site, city, description;
    private Date date;

    protected Event(Parcel in) {
        id = in.readString();
        owner = in.readString();
        speaker = in.readString();
        title = in.readString();
        site = in.readString();
        city = in.readString();
        description = in.readString();
        date = new Date(in.readLong());;
    }

    public Event(String owner, String speaker, String title, String site, String city, String description, Date date) {
        this.owner = owner;
        this.speaker = speaker;
        this.title = title;
        this.site = site;
        this.city = city;
        this.description = description;
        this.date = date;
    }

    public Event(String id, String owner, String speaker, String title, String site, String city, String description, Date date) {
        this.id = id;
        this.owner = owner;
        this.speaker = speaker;
        this.title = title;
        this.site = site;
        this.city = city;
        this.description = description;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", owner='" + owner + '\'' +
                ", speaker='" + speaker + '\'' +
                ", title='" + title + '\'' +
                ", site='" + site + '\'' +
                ", city='" + city + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getDate() {
        String s = new SimpleDateFormat("dd/MM/yyyy").format(this.date);
        System.out.println(s);
        return s;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(owner);
        parcel.writeString(speaker);
        parcel.writeString(title);
        parcel.writeString(site);
        parcel.writeString(city);
        parcel.writeString(description);
        parcel.writeLong(date.getTime());
    }
}
