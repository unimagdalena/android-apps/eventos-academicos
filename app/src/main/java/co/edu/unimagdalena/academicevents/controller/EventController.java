package co.edu.unimagdalena.academicevents.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.edu.unimagdalena.academicevents.Database;
import co.edu.unimagdalena.academicevents.DefDB;
import co.edu.unimagdalena.academicevents.entity.Event;

public class EventController {

    private static Database bd;
    private static Context c;

    public EventController(Context c) {
        this.bd = new Database(c,1);
        this.c = c;
    }

    public Event getEvent(String _id) {
        String args[] = new String[] {_id};
        SQLiteDatabase sql = bd.getReadableDatabase();
        Cursor cursor = sql.query(DefDB.tabla_evt, null, "id=?", args, null, null, null);

        // To increase performance first get the index of each column in the cursor
        final int idIndex = cursor.getColumnIndex("id");
        final int ownerIndex = cursor.getColumnIndex("owner");
        final int speakerIndex = cursor.getColumnIndex("speaker");
        final int titleIndex = cursor.getColumnIndex("title");
        final int siteIndex = cursor.getColumnIndex("site");
        final int cityIndex = cursor.getColumnIndex("city");
        final int dateIndex = cursor.getColumnIndex("date");
        final int descriptionIndex = cursor.getColumnIndex("description");

        try {

            // If moveToFirst() returns false then cursor is empty
            if (!cursor.moveToFirst()) {
                //return new ArrayList<>();
                return null;
            }

            final List<Event> products = new ArrayList<>();

            do {

                // Read the values of a row in the table using the indexes acquired above
                final String id = cursor.getString(idIndex);
                final String owner = cursor.getString(ownerIndex);
                final String speaker = cursor.getString(speakerIndex);
                final String title = cursor.getString(titleIndex);
                final String site = cursor.getString(siteIndex);
                final String city = cursor.getString(cityIndex);
                final String date = cursor.getString(dateIndex);
                final String description = cursor.getString(descriptionIndex);

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                products.add(new Event(id, owner, speaker, title, site, city, description, formatter.parse(date)));

            } while (cursor.moveToNext());

            return products.get(0);

        }
        catch (Exception ex){
            Toast.makeText(c, "Error consulta eventos " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
        finally {
            // Don't forget to close the Cursor once you are done to avoid memory leaks.
            // Using a try/finally like in this example is usually the best way to handle this
            cursor.close();

            // close the database
            sql.close();
        }

    }

    public void addEvent(Event e) {
        try {
            SQLiteDatabase sql = bd.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DefDB.col_title, e.getTitle());
            values.put(DefDB.col_owner, e.getOwner());
            values.put(DefDB.col_site, e.getSite());
            values.put(DefDB.col_city, e.getCity());
            values.put(DefDB.col_description, e.getDescription());
            values.put(DefDB.col_speaker, e.getSpeaker());
            values.put(DefDB.col_date, e.getDate());
            long id = sql.insert(DefDB.tabla_evt, null, values);

            Toast.makeText(c, "Evento registrado", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(c, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void editEvent(Event e) {
        String[] args = new String[]{e.getId()};
        SQLiteDatabase sql = bd.getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            values.put(DefDB.col_id, e.getId());
            values.put(DefDB.col_title, e.getTitle());
            values.put(DefDB.col_owner, e.getOwner());
            values.put(DefDB.col_site, e.getSite());
            values.put(DefDB.col_city, e.getCity());
            values.put(DefDB.col_description, e.getDescription());
            values.put(DefDB.col_speaker, e.getSpeaker());
            values.put(DefDB.col_date, e.getDate());

            sql.update(DefDB.tabla_evt, values, "id=?", args);

            Toast.makeText(c, "Evento editado correctamente", Toast.LENGTH_LONG).show();
        } catch (Exception ex){
            Toast.makeText(c, "Error editando el evento " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void deleteEvent(Event e) {
        String[] args = new String[]{e.getId()};
        SQLiteDatabase sql = bd.getReadableDatabase();
        try {
            boolean res = sql.delete(DefDB.tabla_evt, "id=?", args) > 0;
            if (res) {Toast.makeText(c, "Evento eliminado", Toast.LENGTH_LONG).show();}
        } catch (Exception ex) {
            Toast.makeText(c, "Error eliminando el evento " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public Cursor getEventsCursor() {
        try{
            SQLiteDatabase sql = bd.getReadableDatabase();
            Cursor cur = sql.rawQuery("select id as _id , owner, speaker, title, site, city, description, date from event", null);
            return cur;
        }
        catch (Exception ex){
            Toast.makeText(c, "Error al consultar eventos " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }
}
