package co.edu.unimagdalena.academicevents.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.edu.unimagdalena.academicevents.Database;
import co.edu.unimagdalena.academicevents.DefDB;
import co.edu.unimagdalena.academicevents.entity.Student;

public class StudentController {
    private static Database bd;
    private static Context c;

    public StudentController() {
        this.bd = new Database(c,1);
    }

    public StudentController(Context c) {
        this.bd = new Database(c,1);
        this.c = c;
    }

   /* public StudentController(Database bd, Context c) {
        this.bd = bd;
        this.c = c;
    }

    public static void addStudent(Student e) {
        try {
            SQLiteDatabase sql = bd.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DefDB.col_cod, e.getCod());
            values.put(DefDB.col_name, e.getName());
            values.put(DefDB.col_program, e.getProgram());
            long id = sql.insert(DefDB.tabla_est, null, values);

            Toast.makeText(c, "Estudiante registrado", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(c, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
    public boolean editStudent(Student e) {
        String[] args = new String []{e.getCod()};
        SQLiteDatabase sql = bd.getReadableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("codigo", e.getCod());
        cv.put("nombre", e.getName());
        cv.put("programa", e.getProgram());

        try {
            sql.update(DefDB.tabla_est, cv, "codigo=?", args);
            return true;
        } catch (Exception ex){
            Toast.makeText(c, "Error editando estudiantes " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }

    }

    public boolean deleteStudent(String cod) {
        String args[] = new String[] {cod};
        SQLiteDatabase sql = bd.getReadableDatabase();
        return sql.delete(DefDB.tabla_est, "codigo=?", args) > 0;
    }

    public static boolean findStudent(Student e) {
        String args[] = new String[] {e.getCod()};
        String col[] = new String[] {DefDB.col_cod, DefDB.col_name};
        SQLiteDatabase sql = bd.getReadableDatabase();
        Cursor c = sql.query(DefDB.tabla_est, null, "codigo=?", args, null, null, null);
        if (c.getCount() > 0) {
            bd.close();
            return true;
        } else {
            bd.close();
            return false;
        }
    }

    public Student getStudent(String cod) {
        String args[] = new String[] {cod};
        String col[] = new String[] {DefDB.col_cod, DefDB.col_name};
        SQLiteDatabase sql = bd.getReadableDatabase();
        Cursor cursor = sql.query(DefDB.tabla_est, null, "cod=?", args, null, null, null);

        // To increase performance first get the index of each column in the cursor
        final int codIndex = cursor.getColumnIndex("cod");
        final int nameIndex = cursor.getColumnIndex("name");
        final int programIndex = cursor.getColumnIndex("program");

        try {

            // If moveToFirst() returns false then cursor is empty
            if (!cursor.moveToFirst()) {
                //return new ArrayList<>();
                return null;
            }

            final List<Student> products = new ArrayList<>();

            do {

                // Read the values of a row in the table using the indexes acquired above
                final String name = cursor.getString(nameIndex);
                final String code = cursor.getString(codIndex);
                final String program = cursor.getString(programIndex);

                products.add(new Student(code, name, program));

            } while (cursor.moveToNext());

            return products.get(0);

        }
        catch (Exception ex){
            Toast.makeText(c, "Error consulta estudiantes " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
        finally {
            // Don't forget to close the Cursor once you are done to avoid memory leaks.
            // Using a try/finally like in this example is usually the best way to handle this
            cursor.close();

            // close the database
            sql.close();
        }

    }

    public Cursor getStudentsCursor(){
        try{
            SQLiteDatabase sql = bd.getReadableDatabase();
            Cursor cur = sql.rawQuery("select cod as _id , name, program from student", null);
            return cur;
        }
        catch (Exception ex){
            Toast.makeText(c, "Error al consultar estudiantes " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }*/

}
