package co.edu.unimagdalena.academicevents;

import java.util.Date;

public class DefDB {

    public static final String nameDb = "Universidad";

   /* // tabla de estudiantes para iniciar sesión
    public static final String tabla_est = "student";
    public static final String col_cod = "cod";
    public static final String col_name = "name";
    public static final String col_program = "program";

    public static final String create_tabla_est = "CREATE TABLE IF NOT EXISTS " + DefDB.tabla_est + " ( " +
            DefDB.col_cod + " text primary key," +
            DefDB.col_name + " text," +
            DefDB.col_program + " text" +
            ");";*/

    // tabla de eventos académicos para la lista
    public static final String tabla_evt = "event";
    public static final String col_id = "id";
    public static final String col_owner = "owner";                     // Organizador del evento
    public static final String col_speaker = "speaker";                 // Conferencista
    public static final String col_title = "title";
    public static final String col_site = "site";
    public static final String col_city = "city";
    public static final String col_description = "description";
    public static final String col_date = "date";

    public static final String create_tabla_evt = "CREATE TABLE IF NOT EXISTS " + DefDB.tabla_evt + " ( " +
            DefDB.col_id + " INTEGER primary key AUTOINCREMENT," +
            DefDB.col_owner + " text," +
            DefDB.col_speaker + " text," +
            DefDB.col_title + " text ," +
            DefDB.col_site + " text," +
            DefDB.col_city + " text," +
            DefDB.col_description + " text," +
            DefDB.col_date + " text" +
            ");";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DefDB.tabla_evt;
}
