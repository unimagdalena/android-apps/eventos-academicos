package co.edu.unimagdalena.academicevents;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class StudentCursorAdapter extends CursorAdapter {

    public StudentCursorAdapter(Context context, Cursor cursor, boolean autoRequery) {
        super(context, cursor, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.row_list, parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView cod = view.findViewById(R.id.txt_speaker);
        TextView nom = view.findViewById(R.id.txt_title);
        TextView prg = view.findViewById(R.id.txt_date);
        String codigo = cursor.getString(0);
        String nombre = cursor.getString(1);
        String prog = cursor.getString(2);
        cod.setText(codigo);
        nom.setText(nombre);
        prg.setText(prog);
    }

}
