package co.edu.unimagdalena.academicevents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.QuickContactBadge;

import co.edu.unimagdalena.academicevents.controller.EventController;
import co.edu.unimagdalena.academicevents.entity.Event;

public class MainActivity2 extends AppCompatActivity {
    ListView listado;
    Button agregar;
    EventController eventController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        agregar = findViewById(R.id.btnAgregar);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity2.this,EventActivity.class);
                startActivity(intent);
            }
        });

        listado = findViewById(R.id.lsListado);
        eventController = new EventController(this);
        final Cursor c= eventController.getEventsCursor();
        final EventCursorAdapter ecurso = new EventCursorAdapter(this, c,false);
        listado.setAdapter(ecurso);
        listado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity2.this, MostrarEvento.class);
                /*intent.putExtra("nombre",c.getString(3));
                intent.putExtra("organi",c.getString(1));
                intent.putExtra("confer",c.getString(2));
                intent.putExtra("lugar",c.getString(4));
                intent.putExtra("city",c.getString(5));
                intent.putExtra("descr",c.getString(6));
                intent.putExtra("dad",c.getString(7));*/

                String eventId = c.getString(0);
                Event event = eventController.getEvent(eventId);
                intent.putExtra("event", event);
                startActivity(intent);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            SharedPreferences preferences= getSharedPreferences("login", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor= preferences.edit();
            editor.clear();
            editor.commit();
            Intent intent = new Intent(MainActivity2.this, LoginActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}