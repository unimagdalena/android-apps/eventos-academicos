package co.edu.unimagdalena.academicevents;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventCursorAdapter extends CursorAdapter {

    public EventCursorAdapter(Context context, Cursor cursor, boolean autoRequery) {
        super(context, cursor, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.row_list, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView title = view.findViewById(R.id.txt_title);
        TextView speaker = view.findViewById(R.id.txt_speaker);
        TextView date = view.findViewById(R.id.txt_date);

        String d_speaker = cursor.getString(2);
        String d_title = cursor.getString(3);
        String d_site = cursor.getString(4);
        String d_date = cursor.getString(7);

        title.setText(d_title);
        speaker.setText(d_speaker);
        date.setText(d_site + " " + d_date);
    }
}
