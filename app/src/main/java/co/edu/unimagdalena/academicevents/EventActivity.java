package co.edu.unimagdalena.academicevents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import co.edu.unimagdalena.academicevents.controller.EventController;
import co.edu.unimagdalena.academicevents.entity.Event;


public class EventActivity extends AppCompatActivity {

    private TextInputEditText inputName, inputOwner, inputSpeaker, inputSite, inputCity, inputDate, inputDescription;
    private Button saveEvent;

    private EventController eventController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        eventController = new EventController(this);

        inputName = findViewById(R.id.input_name);
        inputOwner = findViewById(R.id.input_owner);
        inputSpeaker = findViewById(R.id.input_speaker);
        inputSite = findViewById(R.id.input_site);
        inputCity = findViewById(R.id.input_city);
        inputDate = findViewById(R.id.input_date);
        inputDescription = findViewById(R.id.input_description);

        saveEvent = findViewById(R.id.button_save_event);
        saveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameValue = inputName.getText().toString();
                String ownerValue = inputOwner.getText().toString();
                String speakerValue = inputSpeaker.getText().toString();
                String siteValue = inputSite.getText().toString();
                String cityValue = inputCity.getText().toString();
                String dateValue = inputDate.getText().toString();
                String descriptionValue = inputDescription.getText().toString();

                if(TextUtils.isEmpty(nameValue) || TextUtils.isEmpty(ownerValue) || TextUtils.isEmpty(dateValue)){
                    Toast.makeText(getApplicationContext(),"Datos inválidos", Toast.LENGTH_LONG).show();
                } else {

                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    Date parsedDate = null;
                    try {
                        parsedDate = formatter.parse(dateValue);

                        Event event = new Event(ownerValue, speakerValue, nameValue, siteValue, cityValue, descriptionValue, parsedDate);
                        eventController.addEvent(event);

                        Intent intent = new Intent(EventActivity.this, MainActivity2.class);
                        startActivity(intent);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
    }

}