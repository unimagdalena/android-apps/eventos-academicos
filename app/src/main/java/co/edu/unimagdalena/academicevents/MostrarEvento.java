package co.edu.unimagdalena.academicevents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import co.edu.unimagdalena.academicevents.controller.EventController;
import co.edu.unimagdalena.academicevents.entity.Event;

public class MostrarEvento extends AppCompatActivity implements View.OnClickListener {

    TextView nom;
    TextInputEditText organizador, conferencista, ciudad, lugar, descripcion, hora;
    Button actualizar, eliminar;

    Event event;
    EventController eventController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_evento);
        nom = findViewById(R.id.input_name);

        eventController = new EventController(this);

        organizador = findViewById(R.id.input_owner);
        conferencista = findViewById(R.id.input_speaker);
        ciudad= findViewById(R.id.input_city);
        lugar = findViewById(R.id.input_site);
        descripcion = findViewById(R.id.input_description);
        hora = findViewById(R.id.input_date);
        actualizar = findViewById(R.id.btnActualizar);
        eliminar=findViewById(R.id.btnEliminar);

        Bundle bundle = getIntent().getExtras();
        event = bundle.getParcelable("event");

        nom.setText(event.getTitle());
        organizador.setText(event.getOwner());
        conferencista.setText(event.getSpeaker());
        lugar.setText(event.getSite());
        ciudad.setText(event.getCity());
        descripcion.setText(event.getDescription());
        hora.setText(event.getDate());

        actualizar.setOnClickListener(this);
        eliminar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnActualizar:
                /*database = new Database(this,1);
                SQLiteDatabase bd= database.getWritableDatabase();
                ContentValues valores = new ContentValues();
                valores.put(DefDB.col_city, ciudad.getText().toString());
                valores.put(DefDB.col_date, hora.getText().toString());
                valores.put(DefDB.col_description, descripcion.getText().toString());
                valores.put(DefDB.col_owner, organizador.getText().toString());
                valores.put(DefDB.col_speaker, conferencista.getText().toString());
                valores.put(DefDB.col_site, lugar.getText().toString());

                String whereArgs[] = {nom.getText().toString()};
                bd.update(DefDB.tabla_evt,valores,DefDB.col_title+"=?",whereArgs);
                bd.close();
                Toast.makeText(getApplicationContext(), "El evento se actualizado correctamente", Toast.LENGTH_LONG).show();*/
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date date = null;
                try {
                    date = formatter.parse(hora.getText().toString());
                    event.setCity(ciudad.getText().toString());
                    event.setDate(date);
                    event.setDescription(descripcion.getText().toString());
                    event.setOwner(organizador.getText().toString());
                    event.setDescription(descripcion.getText().toString());
                    event.setSpeaker(conferencista.getText().toString());
                    event.setSite(lugar.getText().toString());
                    event.setTitle(nom.getText().toString());
                    System.out.println(event.toString());
                    eventController.editEvent(event);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Intent i = new Intent(this, MainActivity2.class);
                startActivity(i);
                break;
            case R.id.btnEliminar:
                /*database = new Database(this,1);
                SQLiteDatabase db= database.getWritableDatabase();
                db= database.getWritableDatabase();
                String parametro[] = {nom.getText().toString()};
                db.delete(DefDB.tabla_evt,DefDB.col_title+"=?",parametro);
                db.close();
                Toast.makeText(getApplicationContext(), "El evento se Elimino correctamente", Toast.LENGTH_LONG).show();*/

                eventController.deleteEvent(this.event);

                Intent j = new Intent(this, MainActivity2.class);
                startActivity(j);
                break;
        }
    }
}