package co.edu.unimagdalena.academicevents;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.Preference;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout pass, user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user = findViewById(R.id.data1_input);
        pass = findViewById(R.id.data2_input);

        String nombreUser= iniciarlogin("user");
        if(TextUtils.isEmpty(nombreUser)){
            final Button loginButton = findViewById(R.id.login_button);
            loginButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button
                    String userValue = user.getEditText().getText().toString();
                    String passValue = pass.getEditText().getText().toString();
                    if(TextUtils.isEmpty(userValue) || TextUtils.isEmpty(passValue)){
                        Toast.makeText(getApplicationContext(),"Datos inválidos", Toast.LENGTH_LONG).show();
                    } else {
                        guardarPreferencias();

                    }

                }

            });
        }else{
            Intent intent = new Intent(LoginActivity.this, MainActivity2.class);
            startActivity(intent);
        }
    }

    private String iniciarlogin(String user) {
        SharedPreferences preferences= getSharedPreferences("login",Context.MODE_PRIVATE);
        return preferences.getString(user,"");
    }


    private void guardarPreferencias(){
        SharedPreferences preferences= getSharedPreferences("login",Context.MODE_PRIVATE);
        String usuario=user.getEditText().getText().toString();
        String passaword=pass.getEditText().getText().toString();
        SharedPreferences.Editor editor= preferences.edit();
        editor.putString("user",usuario);
        editor.putString("pass",passaword);
        editor.commit();

        Intent intent = new Intent(LoginActivity.this, MainActivity2.class);
        startActivity(intent);

    }
}